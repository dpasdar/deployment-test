# Sample Application To Try Webhook-SQS-Relay

The following is a simple flask application which has a CI/CD pipeline. The pipeline builds a docker image, push it to GitLab container registry and then, sends a webhook
trigger to "pantai-hillpark" address. Provided that somebody (an agent) is listening at this address, a redployment script can be called or some other action can be triggered. For more information refer to the [GitHub repo for Webhook-SQS-Relay](https://github.com/dpasdar/sqs-webhook-sender) 
