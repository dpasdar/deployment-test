FROM python:3
EXPOSE 80
COPY requirements.txt /
RUN pip install --no-cache-dir -r requirements.txt
COPY run.py /
CMD python /run.py
