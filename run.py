#!/usr/bin/env python

import flask, socket, os
app = flask.Flask(__name__)

@app.route("/")
def index():
  return "<body style='background-color:red'>" \
         "<p>Hello from {}</p>" \
         "</body>"\
    .format(socket.gethostname())

app.run(host="0.0.0.0", port= os.environ.get('PORT', '80'))
